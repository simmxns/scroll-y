const blocks = document.getElementsByClassName('block')
const container = document.getElementsByClassName('block__container')

var hs = new HorizontalScroll.default({
	blocks: blocks,
	container: container,
	isAnimated: true,
	springEffect: 0.8,
})
